ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/ubi/ubi8
ARG BASE_TAG=8.5

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

COPY golang.tar.gz /

RUN tar -xzf golang.tar.gz -C /usr/local \
    && rm -f /golang.tar.gz \
    # Delete PEM files used for testing
    && rm -f /usr/local/go/src/crypto/tls/testdata/example-key.pem \
    && useradd -u 1001 -s /bin/bash golanguser \
    && dnf upgrade -y \
    && dnf install -y --nodocs git \
    && dnf clean all \
    && rm -rf /var/cache/dnf \
    && rm /usr/libexec/openssh/ssh-keysign \
    && rm -rf /usr/share/doc/perl-IO-Socket-SSL/certs/

ENV GOROOT=/usr/local/go
ENV GOPATH=/go
ENV PATH=$GOPATH/bin:$GOROOT/bin:$PATH

RUN mkdir -p "$GOPATH/src" "$GOPATH/bin" \
    && chmod -R 744 "$GOPATH" \
    && chown -R 1001:1001 "$GOPATH"

WORKDIR $GOPATH

USER 1001
HEALTHCHECK NONE
