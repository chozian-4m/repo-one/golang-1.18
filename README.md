# golang 1.18.x

# Description

Go is an open source programming language that makes it easy to build simple, reliable, and efficient software.

# Usage

## Start a Go instance in your app

The most straightforward way to use this image is to use a Go container as both the build and runtime environment. In your Dockerfile, writing something along the lines of the following will compile and run your project:

```
FROM golang:1.18

WORKDIR /go/src/app
COPY . .

RUN go get -d -v ./...
RUN go install -v ./...

CMD ["app"]
```

You can then build and run the Docker image:

```
$ docker build -t my-golang-app .
$ docker run -it --rm --name my-running-app my-golang-app
```

## Compile your app inside the Docker container

There may be occasions where it is not appropriate to run your app inside a container. To compile, but not run your app inside the Docker instance, you can write something like:

```
$ docker run --rm -v "$PWD":/usr/src/myapp -w /usr/src/myapp golang:1.18 go build -v
```

This will add your current directory as a volume to the container, set the working directory to the volume, and run the command go build which will tell go to compile the project in the working directory and output the executable to myapp. Alternatively, if you have a Makefile, you can run the make command inside your container.

```
$ docker run --rm -v "$PWD":/usr/src/myapp -w /usr/src/myapp golang:1.18 make
```

## Cross-compile your app inside the Docker container

If you need to compile your application for a platform other than linux/amd64 (such as windows/386):

```
$ docker run --rm -v "$PWD":/usr/src/myapp -w /usr/src/myapp -e GOOS=windows -e GOARCH=386 golang:1.18 go build -v
```

Alternatively, you can build for multiple platforms at once:

```
$ docker run --rm -it -v "$PWD":/usr/src/myapp -w /usr/src/myapp golang:1.18 bash
$ for GOOS in darwin linux; do
>   for GOARCH in 386 amd64; do
>     export GOOS GOARCH
>     go build -v -o myapp-$GOOS-$GOARCH
>   done
> done
```
